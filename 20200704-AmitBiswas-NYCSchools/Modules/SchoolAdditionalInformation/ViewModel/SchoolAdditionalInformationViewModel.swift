//
//  SchoolAdditionalInformationViewModel.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import Foundation

typealias SchoolsAdditionalInfoViewModelOutput = (SchoolAdditionalInformationViewModel.Output) -> ()

class SchoolAdditionalInformationViewModel {
    
    var school: School
    var satScore: SATScore?
    
    var completionHandler: SchoolsAdditionalInfoViewModelOutput?
    
    //MARK: - Initializer
    init(school: School) {
        self.school = school
    }
    
    enum Output {
        case updateData
        case showLoader
        case hideLoader
        case showError(message: String)
    }
    
}

//MARK: - Api Call
extension SchoolAdditionalInformationViewModel {
    
    func fetchSATScores() {
        let isSATScoresAlreadyFetched = AppConstants.allSatScores.count != 0
        
        if isSATScoresAlreadyFetched {
        
            self.findSATResultOfSchool(allSatScores: AppConstants.allSatScores)
        
        } else {
            
            completionHandler?(.showLoader)
            NetworkingManager.shared.fetchSATScores { result in
                
                self.completionHandler?(.hideLoader)
                
                switch result {
                case.success(let satScoreList):
                    AppConstants.allSatScores.removeAll()
                    AppConstants.allSatScores.append(contentsOf: satScoreList)
                    
                    self.findSATResultOfSchool(allSatScores: satScoreList)
                    
                case .failure(let error):
                    self.completionHandler?(.showError(message: error.rawValue))
                }
            }
        }
    }
    
    private func findSATResultOfSchool(allSatScores: [SATScore]) {
        if let satScore = allSatScores.first(where: { $0.dbn == self.school.dbn }) {
            self.satScore = satScore
            self.completionHandler?(.updateData)
        }
    }
    
}
