//
//  SchoolsListViewModel.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import Foundation


typealias SchoolsListViewModelOutput = (SchoolsListViewModel.Output) -> ()

class SchoolsListViewModel {

    private var cellViewModels = [SchoolCellListViewModel]()
    
    private var schoolsList: [School] {
        didSet {
            initializeCellViewModels()
            completionHandler?(.reloadData)
        }
    }
    
    var completionHandler: SchoolsListViewModelOutput?
    
    //MARK: - Initializer
    init(schoolsList: [School]) {
        self.schoolsList = schoolsList
    }
    
    enum Output {
        case reloadData
        case showLoader
        case hideLoader
        case showError(message: String)
    }

}

//MARK: - Api Call
extension SchoolsListViewModel {
    
    func fetchSchoolList() {
        completionHandler?(.showLoader)
        
        NetworkingManager.shared.fetchSchoolsList { result in
            
            self.completionHandler?(.hideLoader)
            
            switch result {
            case.success(let schools):
                self.schoolsList = schools
                
            case .failure(let error):
                self.completionHandler?(.showError(message: error.rawValue))
            }
            
        }
    }
    
}


//MARK: - Helper Methods
extension SchoolsListViewModel {
    
    private func initializeCellViewModels() {
        cellViewModels = []
        
        for (index, school) in self.schoolsList.enumerated() {
            
            let schoolCellVM = SchoolCellListViewModel(cellIndex: index,
                                                       schoolName: school.schoolName ?? "",
                                                       schoolCity: school.city ?? "")
            self.cellViewModels.append(schoolCellVM)
        }
        
    }
    
    var rows: Int {
        return schoolsList.count
    }
    
    func cellViewModel(for row: Int) -> SchoolCellListViewModel {
        return cellViewModels[row]
    }

    func getSchoolFor(index: Int) -> School {
        return self.schoolsList[index]
    }
    
}
