//
//  SchoolsListViewController.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import UIKit

protocol SchoolListDelegate: class {
    func didTapSchool(_ school: School)
}

class SchoolsListViewController: UIViewController {

    private let navBarTitle = "NYC High Schools"
    private let cellIdentifier = "schoolListCell"
    private let schoolNibName = "SchoolTableViewCell"
    
    weak var delegate: SchoolListDelegate?
    
    //MARK: - ViewModel
    var viewModel: SchoolsListViewModel?
    
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
        
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = self.navBarTitle
        self.registerNib()
        self.viewModelBinding()
        self.fetchSchools()
    }
    
}

//MARK: - View Model Binding
extension SchoolsListViewController {
    
    private func viewModelBinding() {
        
        self.viewModel?.completionHandler = { output in
            switch output {
            case .reloadData:
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            case .hideLoader:
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                
            case .showLoader:
                DispatchQueue.main.async {
                    self.activityIndicator.startAnimating()
                }
                
            case .showError(let message):
                DispatchQueue.main.async {
                    AppConstants.showAlert(withTitle: "Error", Message: message, controller: self)
                }
            }
        }
    }
    
}

//MARK: - Private Methods
extension SchoolsListViewController {
    
    private func fetchSchools() {
        self.viewModel?.fetchSchoolList()
    }
    
    private func registerNib() {
        let nib = UINib(nibName: schoolNibName, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    }
    
}


//MARK: - TableViewDelegate
extension SchoolsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SchoolTableViewCell,
              let viewModel = self.viewModel else {
            return UITableViewCell()
        }
        
        cell.congifureViewModel(viewModel: viewModel.cellViewModel(for: indexPath.row))
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.rows ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = self.viewModel else {
            return 
        }
  
        self.delegate?.didTapSchool(viewModel.getSchoolFor(index: indexPath.row))
    }
    
}
