//
//  SchoolTableViewCell.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    var viewModel: SchoolCellListViewModel?
    
    //MARK: - Outlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolLocationLabel: UILabel!
    
    
    //MARK: - Public Function
    func congifureViewModel(viewModel: SchoolCellListViewModel) {
        self.viewModel = viewModel
        
        guard let viewModel = self.viewModel else {
            return
        }
        
        self.setData(schoolName: viewModel.schoolName,
                     schoolCity: viewModel.schoolCity)
    }
    
    
    //MARK: - Private Methods
    func setData(schoolName: String, schoolCity: String) {
        self.schoolNameLabel.text = schoolName
        self.schoolLocationLabel.text = schoolCity
    }
    
}
