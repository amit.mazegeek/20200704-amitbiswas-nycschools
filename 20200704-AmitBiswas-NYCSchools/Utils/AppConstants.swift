//
//  AppConstants.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import UIKit


struct AppConstants {
        
    
    //MARK: -Global Variable
    static var allSatScores = [SATScore]()
    
    
    //MARK: - End Points
    struct EndPoints {
        static let schoolsList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let SATScoresList = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    
    //MARK: - Helper Methods
    static func showAlert(withTitle title: String, Message message: String, controller: UIViewController) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: title, style: .default))
        controller.present(ac, animated: true)
    }
    
}
