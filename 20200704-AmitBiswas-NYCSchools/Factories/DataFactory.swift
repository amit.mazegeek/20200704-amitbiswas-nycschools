//
//  DataFactory.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import UIKit

class DataFactory {
    
    static func getSchoolsListViewController() -> SchoolsListViewController? {
        let mainStoryboard = UIStoryboard(storyboard: .main)
        if let schoolsListViewController = mainStoryboard.instantiateViewController(withIdentifier: "SchoolsListViewController") as? SchoolsListViewController {
            schoolsListViewController.viewModel = SchoolsListViewModel(schoolsList: [School]())
            
            return schoolsListViewController
        }
        
        return nil
    }
    
    
    static func getSchoolAdditionalInfoVC(_ school: School) -> SchoolInformationViewController? {
        let mainStoryboard = UIStoryboard(storyboard: .main)
        if let schoolsInfoVC = mainStoryboard.instantiateViewController(withIdentifier: "SchoolInformationViewController") as? SchoolInformationViewController {
            schoolsInfoVC.viewModel = SchoolAdditionalInformationViewModel(school: school)
            return schoolsInfoVC
        }
        
        return nil
    }
    
}
