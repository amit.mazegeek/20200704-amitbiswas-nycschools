//
//  NetworkingManager.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 7/4/21.
//

import Foundation

enum NetworkError: String, Error {
    case serverError = "Server Error"
}

final class NetworkingManager {
    
    //MARK: - Shared Instance
    static let shared = NetworkingManager()
    
    
    //MARK: - Initializers
    private init () { }
        
    
    //MARK: - SchoolsList Api
    func fetchSchoolsList(completionHandler: @escaping (Result<[School], NetworkError>) -> Void ) {
        
        guard let url = URL(string: AppConstants.EndPoints.schoolsList) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard let data = data else {
                completionHandler(.failure(.serverError))
                return
            }
            
            do {
                
                let schoolList = try JSONDecoder().decode([School].self, from: data)
                completionHandler(.success(schoolList))
                return
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        })
        task.resume()
    }
    
    
    //MARK: - SAT Scores Api
    func fetchSATScores(completionHandler: @escaping (Result<[SATScore], NetworkError>) -> Void ) {
        
        guard let url = URL(string: AppConstants.EndPoints.SATScoresList) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard let data = data else {
                completionHandler(.failure(.serverError))
                return
            }
            
            do {
                
                let SATScores = try JSONDecoder().decode([SATScore].self, from: data)
                completionHandler(.success(SATScores))
                return
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        })
        task.resume()
    }
    
}
