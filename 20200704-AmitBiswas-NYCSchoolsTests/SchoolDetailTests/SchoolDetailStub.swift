//
//  SchoolDetailStub.swift
//  20200704-AmitBiswas-NYCSchoolsTests
//
//  Created by Amit Biswas on 7/4/21.
//

import Foundation

let SATScoreStub = """
{
"dbn": "01M448",
"school_name": "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL",
"num_of_sat_test_takers": "91",
"sat_critical_reading_avg_score": "383",
"sat_math_avg_score": "423",
"sat_writing_avg_score": "366"
}
"""
